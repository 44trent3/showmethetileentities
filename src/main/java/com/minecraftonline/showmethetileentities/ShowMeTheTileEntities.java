package com.minecraftonline.showmethetileentities;

import com.google.common.collect.Lists;
import com.google.common.collect.Streams;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.pagination.PaginationService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Plugin(
        id = "showmethetileentities",
        name = "ShowMeTheTileEntities",
        description = "Show me the tile entities!",
        authors = {
                "44trent3"
        }
)
public class ShowMeTheTileEntities {

    @Inject
    private Logger logger;

    @Listener
    @SuppressWarnings("UnstableApiUsage")
    public void onServerStart(GameStartedServerEvent event) {
        logger.info("Loaded ShowMeTheTileEntities.");
        CommandSpec spec = CommandSpec.builder()
                .permission("showmethetileentities")
                .executor((src, args) -> {
                    // Get the world
                    final World world;
                    if (src instanceof Player) {
                        world = ((Player) src).getWorld();
                    }
                    else {
                        world = Sponge.getServer().getWorld(Sponge.getServer().getDefaultWorldName())
                                .orElseThrow(() -> new CommandException(Text.of("Cannot find default world!")));
                    }
                    List<Chunk> chunkToSort = world.isLoaded() ? Lists.newArrayList(world.getLoadedChunks()) : Sponge.getServer().getWorlds().stream().flatMap(world1 -> Streams.stream(world1.getLoadedChunks())).collect(Collectors.toList());
                    TreeMap<Chunk, Integer> sortedChunks = new TreeMap<>((o1, o2) -> Integer.compare(o2.getTileEntities().size(), o1.getTileEntities().size()));
                     for (Chunk chunk : chunkToSort) {
                         sortedChunks.put(chunk, chunk.getTileEntities().size());
                     }
                     List<Text> texts = new ArrayList<>();
                     sortedChunks.forEach(((chunk, integer) -> texts.add(
                             Text.builder().append(Text.of(chunk.getPosition().getX() + "," + chunk.getPosition().getZ() + " contains " + integer + " tiles.")).build())));
                     Sponge.getServiceManager().provideUnchecked(PaginationService.class).builder()
                             .contents((texts))
                             .title(
                                     Text.builder().color(TextColors.GOLD)
                                     .append(Text.of("Show me the tile entities!"))
                                     .build())
                             .sendTo(src);
                    return CommandResult.success();
                })
                .build();
        Sponge.getCommandManager().register(this, spec, "showmethetileentities");
    }
}
